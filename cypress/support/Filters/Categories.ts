export enum CategoryPL {
  WszystkieKategorie = "Wszystkie kategorie",
  Sztuka = "Arts & crafts",
  Biuro = "Biuro",
  Biznes = "Biznes, przemysł i nauka",
  Dom = "Dom i kuchnia",
  Dziecko = "Dziecko",
  Elektronika = "Elektronika",
  FilmySeriale = "Filmy i programy TV",
}

export enum CategoryEN {
  Office = "office",
  Electronics = "electronics",
}
