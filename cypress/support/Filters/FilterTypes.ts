export enum FilterTypes {
  Category = "Kategoria",
  ClientRattings = "Recenzja klienta",
  FeaturedBrands = "Wyróżnione marki",
  Price = "Cena",
  ReleaseYear = "Rok modelu",
  MobileNetwork = "Technologia komórkowa",
}
