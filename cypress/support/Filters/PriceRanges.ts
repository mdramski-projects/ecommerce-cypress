export enum PriceRange {
  Below20 = "Do 20 zł",
  From20To50 = "20 do 50 zł",
  From50To100 = "50 do 100 zł",
  From100To150 = "100 do 150 zł",
  Above150 = "150 zł i więcej",
}
