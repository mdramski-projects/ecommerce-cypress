export class SubNavigationBar {
  sub_navigation_bar = "#nav-subnav";

  assertSubBarCategory(category: string): void {
    cy.get(this.sub_navigation_bar)
      .invoke("attr", "data-category")
      .should("eq", category);
  }
}
