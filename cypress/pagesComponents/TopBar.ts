export class TopBar {
  search_input = "input#twotabsearchtextbox";
  search_bar_category_dropdown = "#searchDropdownBox";
  search_button = "#nav-search-submit-button";

  selectCategory(category: string) {
    cy.get(this.search_bar_category_dropdown).select(category);
  }

  search(input: string = "") {
    if (input !== "") {
      cy.get(this.search_input).type(input);
    }
    cy.get(this.search_button).click();
  }
}
