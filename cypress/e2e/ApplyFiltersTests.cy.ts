import { CategoryPL } from "../support/Filters/Categories";
import { HomePage } from "../pages/HomePage";
import { ElectronicsProductsPage } from "../pages/ElectronicsProductsPage";
import { FilterTypes } from "../support/Filters/FilterTypes";
import { ProductPage } from "../pages/ProductPage";

describe("Filters tests suite", () => {
  let homePage: HomePage;
  let electronicsProductsPage: ElectronicsProductsPage;
  let productPage: ProductPage;
  let product: any;

  before(() => {
    homePage = new HomePage();
    electronicsProductsPage = new ElectronicsProductsPage();
    cy.fixture("ProductData.json").then((PD) => {
      product = PD.ExampleProduct;
    });
  });

  beforeEach(() => {
    // Arrange
    homePage.open();
    // Act
    homePage.TopBar.selectCategory(CategoryPL.Elektronika);
    homePage.TopBar.search(product.Name);
    electronicsProductsPage.addFilterWithOption(
      FilterTypes.Price,
      product.Filters.Price,
    );
    electronicsProductsPage.addFilterWithOption(
      FilterTypes.FeaturedBrands,
      product.Filters.Brand,
    );
    electronicsProductsPage.addFilterWithOption(
      FilterTypes.ReleaseYear,
      product.Filters.ReleaseYear,
    );
    electronicsProductsPage.addFilterWithOption(
      FilterTypes.MobileNetwork,
      product.Filters.MobileNetwork,
    );
  });

  it("Should apply selected filters for electronics product and verify filters", () => {
    // Assert
    electronicsProductsPage.assertAppliedFilters(
      product.Filters.Price,
      product.Filters.Brand,
      product.Filters.ReleaseYear,
      product.Filters.MobileNetwork,
    );
  });

  it("Should apply selected filters for electronics product and verify item match filters", () => {
    // Arrange
    productPage = new ProductPage();
    // Assert
    electronicsProductsPage.assertResultGridExists();
    // Act
    electronicsProductsPage.openItemFromResults(1);
    // Assert
    productPage.assertProductDetail("Marka", product.Filters.Brand);
    productPage.assertProductDetail("Rok modelu", product.Filters.ReleaseYear);
    productPage.assertProductDetail(
      "Cellular technology",
      product.Filters.MobileNetwork,
    );
    productPage.assertProductPrice(product.Filters.Price);
  });
});
