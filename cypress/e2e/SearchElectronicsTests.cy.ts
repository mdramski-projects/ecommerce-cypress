import { CategoryPL } from "../support/Filters/Categories";
import { HomePage } from "../pages/HomePage";
import { ElectronicsProductsPage } from "../pages/ElectronicsProductsPage";

describe("Electronics tests suite", () => {
  let homePage: HomePage;
  let electronicsProductsPage: ElectronicsProductsPage;
  let product: any;

  before(() => {
    homePage = new HomePage();
    electronicsProductsPage = new ElectronicsProductsPage();
    cy.fixture("ProductData.json").then((PD) => {
      product = PD.ExampleProduct;
    });
  });

  it("Should open Electronics products main page using search bar", () => {
    // Arrange
    homePage.open();
    // Act
    homePage.TopBar.selectCategory(CategoryPL.Elektronika);
    homePage.TopBar.search();
    // Assert
    electronicsProductsPage.assertPageIsElectronics();
  });

  it("Should find product from electronics category using search bar", () => {
    // Arrange
    homePage.open();
    // Act
    homePage.TopBar.selectCategory(CategoryPL.Elektronika);
    homePage.TopBar.search(product.Name);
    // Assert
    electronicsProductsPage.assertSubBarCategoryIsElectronics();
    electronicsProductsPage.assertSearchResult(product.Name);
  });
});
