import { TopBar } from "../pagesComponents/TopBar";
import { BasePage } from "./BasePage";

export class HomePage extends BasePage {
  TopBar = new TopBar();

  open() {
    super.open("");
  }
}
