export class BasePage {
  accept_cookies_button = "#sp-cc-accept";

  open(path = "") {
    cy.clearAllCookies();
    cy.clearAllLocalStorage();
    cy.clearAllSessionStorage();

    cy.visit(`/${path}`);
    cy.get(this.accept_cookies_button).click();
  }
}
