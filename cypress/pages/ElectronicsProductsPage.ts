import { CategoryEN, CategoryPL } from "../support/Filters/Categories";
import { SubNavigationBar } from "../pagesComponents/SubNavigationBar";
import { BasePage } from "./BasePage";

export class ElectronicsProductsPage extends BasePage {
  subNavigationBar = new SubNavigationBar();

  title_banner = ".pageBanner";
  result_info_bar = 'span[data-component-type="s-result-info-bar"]';
  filter_option_checkbox = (filter: string, option: string) =>
    `div:has(span:contains("${filter}"))+ul>span>span>li:contains("${option}")>span>a>div>label>input`;
  applied_filters = 'div:has(span:contains("Wybrane filtry"))+div>ul';
  applied_filter = (option: string) =>
    `span>li>span>a>span:contains("${option}")`;
  search_result_grid = ".s-result-list";
  search_result_item = (n: number) =>
    `div[data-component-type="s-search-result"]>div>div>span>div>div>div>span>a:eq(${n})`;

  addFilterWithOption(filterName: string, optionName: string) {
    cy.get(this.filter_option_checkbox(filterName, optionName)).click({
      force: true,
    });
  }

  openItemFromResults(selectedItemNumber: number) {
    cy.get(this.search_result_grid)
      .find(this.search_result_item(selectedItemNumber))
      .click();
  }

  assertPageIsElectronics() {
    cy.get(this.title_banner).should("contain.text", CategoryPL.Elektronika);
    this.assertSubBarCategoryIsElectronics();
  }

  assertSubBarCategoryIsElectronics() {
    this.subNavigationBar.assertSubBarCategory(CategoryEN.Electronics);
  }

  assertSearchResult(searchPhrase: string) {
    cy.get(this.result_info_bar).should("contain.text", searchPhrase);
  }

  assertAppliedFilters(...params: string[]) {
    for (let param of params) {
      cy.get(this.applied_filters)
        .find(this.applied_filter(param))
        .should("exist");
    }
  }

  assertResultGridExists() {
    cy.get(this.search_result_grid).should("exist");
  }
}
