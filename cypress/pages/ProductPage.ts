import { PriceRange } from "../support/Filters/PriceRanges";
import { BasePage } from "./BasePage";

export class ProductPage extends BasePage {
  product_details_grid = "#prodDetails";
  price_label_span = "#centerCol .a-price>span>.a-price-whole:first-child";

  assertProductDetail(detailName: string, detailValue: string) {
    cy.get(this.product_details_grid).should("exist").scrollIntoView();
    cy.get(this.product_details_grid)
      .contains(detailName)
      .next(`td`)
      .contains(detailValue, { matchCase: false });
  }

  assertProductPrice(min: number, max: number): void;
  assertProductPrice(priceRange: PriceRange): void;
  assertProductPrice(arg1: number | PriceRange, arg2?: number): void {
    let min: number;
    let max: number;

    if (typeof arg1 === "number" && typeof arg2 === "number") {
      min = arg1;
      max = arg2;
    } else if (typeof arg1 === "string") {
      switch (arg1) {
        case PriceRange.Below20:
          min = 0;
          max = 20;
          break;
        case PriceRange.From20To50:
          min = 20;
          max = 50;
          break;
        case PriceRange.From50To100:
          min = 50;
          max = 100;
          break;
        case PriceRange.From100To150:
          min = 100;
          max = 150;
          break;
        case PriceRange.Above150:
          min = 150;
          max = Infinity;
          break;
        default:
          throw new Error(`Unknown price range: ${arg1}`);
      }
    } else {
      throw new Error("Invalid arguments provided");
    }

    cy.get(this.price_label_span)
      .invoke("text")
      .then((text) => {
        const priceText = text.trim();
        const priceValue = parseInt(priceText.replace(/\D+/g, ""));
        expect(priceValue).to.be.at.least(min);
        expect(priceValue).to.be.at.most(max);
      });
  }
}
